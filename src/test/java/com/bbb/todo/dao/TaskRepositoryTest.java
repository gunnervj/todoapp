package com.bbb.todo.dao;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.bbb.todo.dao.beans.ToDoTask;
import com.bbb.todo.util.CleafDateUtil;

import org.assertj.core.api.Assertions;
import org.assertj.core.api.ThrowableAssert.ThrowingCallable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@TestMethodOrder(OrderAnnotation.class)
public class TaskRepositoryTest {
    
    TaskRepository repository;
    List<ToDoTask> tasks = new ArrayList<>();
    private final Logger log = LoggerFactory.getLogger(TaskRepositoryTest.class);
    Map<String, Integer> userMsgsCountMap = new HashMap<>();
   
    @BeforeEach
    public void init() {
        log.info("Init");
        repository = new TaskRepository("http://localhost:8000", Regions.AP_SOUTH_1);
        initData();
        initCounts();
        log.debug("Init Complete");
      
    }

    private void initData() {

        ToDoTask task = new ToDoTask();
        task.setUserId("VJ001S");
        task.setDescription("Get milk from target. Get vidamin d milk. ");
        task.setStatus("OPEN");
        task.setTaskDate(CleafDateUtil.getTodaysDateAsString());
        task.setTaskId("1");
        task.setTitle("Get Milk");
        tasks.add(task);

        ToDoTask task1 = new ToDoTask();
        task1.setUserId("VJ001S");
        task1.setDescription("Do Laundry from the laundry basket. Kept in kitchen.");
        task1.setStatus("OPEN");
        task1.setTaskDate(CleafDateUtil.getTodaysDateAsString());
        task1.setTaskId("2");
        task1.setTitle("Laundry");
        tasks.add(task1);

        ToDoTask task2 = new ToDoTask();
        task2.setUserId("JO0012");
        task2.setDescription("Talk to bring data into the Coffee Leaf To-do list. Explain merits of making list");
        task2.setStatus("OPEN");
        task2.setTaskDate(CleafDateUtil.getTodaysDateAsString());
        task2.setTaskId("1");
        task2.setTitle("MEeting with VJ");
        tasks.add(task2);

        ToDoTask task3 = new ToDoTask();
        task3.setUserId("JO0012");
        task3.setDescription("Make noodles.");
        task3.setStatus("OPEN");
        task3.setTaskDate(CleafDateUtil.getYesterdaysDateAsString());
        task3.setTaskId("2");
        task3.setTitle("Prepare food");
        tasks.add(task3);     
    }

    private void initCounts() {
        for (ToDoTask task : tasks) {
            if (!CleafDateUtil.isDateInPast(task.getTaskDate())) {
                userMsgsCountMap.put(task.getUserId(), userMsgsCountMap.get(task.getUserId()) == null ? 1 : userMsgsCountMap.get(task.getUserId())+1);
            }
        }
    }


    @Test
    @Order(1)   
    public void testCreateTask() {
        for (ToDoTask task: tasks) {
            ThrowingCallable createTask = () -> {
                repository.createTask(task);
            };
            Assertions.assertThatCode(createTask).doesNotThrowAnyException();
        }
    }

    @Test
    @Order(2)
    public void testGetTask() {
        for (ToDoTask task: tasks) {
            Optional<ToDoTask> taskOpt1 = repository.getTaskDetails(task.getUserId(), task.getTaskId());

            assertTrue(taskOpt1.isPresent());
            assertEquals(taskOpt1.get().getTaskId(), task.getTaskId());
            assertEquals(taskOpt1.get().getTitle(), task.getTitle());
        }
    }


    @Test
    @Order(3)
    public void testGetAllTasksForUser() {
        userMsgsCountMap.forEach((userId, count) -> {
            Optional<List<ToDoTask>> taskOpt1 = repository.getAllTasksForUser(userId);
            assertTrue(taskOpt1.isPresent());
            assertThat(taskOpt1.get().size()).isEqualTo(count);
        });
    }



    @Test
   @Order(4) 
    public void testDeleteTask() {
        for (ToDoTask task: tasks) {
            ThrowingCallable deleteTask = () -> {
                repository.deleteTask(task.getUserId(), task.getTaskId());
            };
            Assertions.assertThatCode(deleteTask).doesNotThrowAnyException();
        }
    }
    
}