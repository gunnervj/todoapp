package com.bbb.exceptions;

import com.bbb.todo.model.ErrorConstants;
import com.bbb.todo.model.HttpCodes;

import lombok.Getter;

@Getter
public class DataException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private ErrorConstants error;
    private HttpCodes httpCode;

    public DataException(ErrorConstants error) {
        super(error.getMessage());
        this.error = error;
        this.httpCode = HttpCodes.INTERNALSERVER_ERROR;
    }

    

}