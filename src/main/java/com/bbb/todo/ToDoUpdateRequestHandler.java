package com.bbb.todo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.bbb.exceptions.DataException;
import com.bbb.todo.dao.ITaskRepository;
import com.bbb.todo.dao.TaskRepository;
import com.bbb.todo.dao.beans.ToDoTask;
import com.bbb.todo.model.ErrorConstants;
import com.bbb.todo.model.HttpCodes;
import com.bbb.todo.model.ToDoCreateUpdateResponse;
import com.bbb.todo.model.ToDoStatus;
import com.bbb.todo.util.CleafDateUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ToDoUpdateRequestHandler implements RequestHandler<Map<String, String>, String> {
    private Gson gson;

    public ToDoUpdateRequestHandler() {
        this.gson = new GsonBuilder().setPrettyPrinting().create();
    }

    @Override
    public String handleRequest(Map<String, String> event, Context context) {
        String regionDynamoDB = System.getenv().get(TaskConstants.DYNAMO_REGION);
        String tableNameDynamoDB =  System.getenv().get(TaskConstants.TASK_TABLE_NAME);
        List<ErrorConstants> errors = new ArrayList<>();
        String response = "{}";

        validateIncomingRequest(errors, event);
        if (!errors.isEmpty()) {
            throw new RuntimeException(ErrorHandler.handleErrorResponse(HttpCodes.BAD_REQUEST, errors));
        }
        try {
            ITaskRepository repository = new TaskRepository(regionDynamoDB, tableNameDynamoDB);
            Optional<ToDoTask> taskOpt = repository.getTaskDetails(event.get(TaskConstants.REQUEST_FIELD_NAME_USER_ID),
                    event.get(TaskConstants.REQUEST_FIELD_NAME_TASK_ID));

            if (taskOpt.isPresent()) {
                ToDoTask task = taskOpt.get();

                if (null != event.get(TaskConstants.REQUEST_FIELD_NAME_DESCRIPTION)
                        && event.get(TaskConstants.REQUEST_FIELD_NAME_DESCRIPTION).trim().length() > 0) {
                    task.setDescription(event.get(TaskConstants.REQUEST_FIELD_NAME_DESCRIPTION));
                }
                if (null != event.get(TaskConstants.REQUEST_FIELD_NAME_STATUS)
                        && event.get(TaskConstants.REQUEST_FIELD_NAME_STATUS).trim().length() > 0) {
                    task.setStatus(ToDoStatus.translateStatus(event.get(TaskConstants.REQUEST_FIELD_NAME_STATUS)));
                }
                if (null != event.get(TaskConstants.REQUEST_FIELD_NAME_TASK_DATE)
                        && event.get(TaskConstants.REQUEST_FIELD_NAME_TASK_DATE).trim().length() > 0) {
                    task.setTaskDate(event.get(TaskConstants.REQUEST_FIELD_NAME_TASK_DATE));
                }
                if (null != event.get(TaskConstants.REQUEST_FIELD_NAME_TITLE)
                        && event.get(TaskConstants.REQUEST_FIELD_NAME_TITLE).trim().length() > 0) {
                    task.setTitle(event.get(TaskConstants.REQUEST_FIELD_NAME_TITLE));
                }
                repository.updateTask(task);
                response = gson.toJson(ToDoCreateUpdateResponse.builder().status(HttpCodes.OK.getCode())
                                .message(HttpCodes.OK.getMessage())
                                .task(task).build());
            } else {
                errors.add(ErrorConstants.TASK_NOT_FOUND);
                throw new RuntimeException(ErrorHandler.handleErrorResponse(HttpCodes.NOT_FOUND, errors));
            }

        } catch (DataException ex) {
            errors.add(ex.getError());
            throw new RuntimeException(ErrorHandler.handleErrorResponse(HttpCodes.INTERNALSERVER_ERROR, errors));
        } catch (Exception ex) {
            errors.add(ErrorConstants.PROCESSING_ERROR);
            throw new RuntimeException(ErrorHandler.handleErrorResponse(HttpCodes.INTERNALSERVER_ERROR, errors));
        }

        log.info("Response = > " + response);
        return response;
    }

    private void validateIncomingRequest(List<ErrorConstants> errors, Map<String, String> event) {

        if (null == event.get(TaskConstants.REQUEST_FIELD_NAME_USER_ID)
                || event.get(TaskConstants.REQUEST_FIELD_NAME_USER_ID).trim().length() == 0) {
            errors.add(ErrorConstants.UNAUTHORIZED);
            throw new RuntimeException(ErrorHandler.handleErrorResponse(HttpCodes.UNAUTHORIZED, errors));
        }

        if (null == event.get(TaskConstants.REQUEST_FIELD_NAME_TASK_ID)
                || event.get(TaskConstants.REQUEST_FIELD_NAME_TASK_ID).trim().length() == 0) {
            errors.add(ErrorConstants.TASK_MISSING);
        }

        if (null != event.get(TaskConstants.REQUEST_FIELD_NAME_STATUS)
                && event.get(TaskConstants.REQUEST_FIELD_NAME_STATUS).trim().length() > 0
                && ToDoStatus.translateStatus(event.get(TaskConstants.REQUEST_FIELD_NAME_STATUS))
                        .equals(ToDoStatus.ERROR.name())) {
            errors.add(ErrorConstants.INVALID_STATUS);
        }

        String taskDate = event.get(TaskConstants.REQUEST_FIELD_NAME_TASK_DATE);
        if (null != taskDate && taskDate.trim().length() > 0) {
            taskDate = taskDate.trim() + TaskConstants.TIME_FORMAT;
            event.put(TaskConstants.REQUEST_FIELD_NAME_TASK_DATE, taskDate);
            if (!CleafDateUtil.isDateStringValid(taskDate)) {
                errors.add(ErrorConstants.TASK_DATE_INVALID_FORMAT);
            }
            if (CleafDateUtil.isDateInPast(taskDate)) {
                errors.add(ErrorConstants.TASK_DATE_INVALID_PAST);
            }
        }
    }

}