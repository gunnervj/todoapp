package com.bbb.todo;

public class TaskConstants {
    public static final String TIME_FORMAT = "T00:00:00.000Z";
    public static final String DYNAMO_REGION = "DYNAMO_REGION";
    public static final String TASK_TABLE_NAME = "TASK_TABLE";
    public static final String COGNITO_IDENTITY_POOL_ID = "COGNITO_IDENTITY_POOL_ID";
    public static final String COGNITO_REGION = "COGNITO_REGION";

    public static final String REQUEST_FIELD_NAME_USER_ID = "userId";
    public static final String REQUEST_FIELD_NAME_TITLE =  "title";
    public static final String REQUEST_FIELD_NAME_DESCRIPTION = "description";
    public static final String REQUEST_FIELD_NAME_TASK_DATE = "taskDate";
    public static final String REQUEST_FIELD_NAME_TASK_ID = "taskId";
    public static final String REQUEST_FIELD_NAME_STATUS = "status";

    public static final String TODAY = "Today";
    public static final String TOMORROW = "Tomorow";
    public static final String FUTURE = "Future";
    
}