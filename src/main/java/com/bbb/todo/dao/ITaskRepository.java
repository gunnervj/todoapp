package com.bbb.todo.dao;

import java.util.List;
import java.util.Optional;

import com.bbb.todo.dao.beans.ToDoTask;

public interface ITaskRepository {

    Optional<List<ToDoTask>> getAllTasksForUser(String userId);
    Optional<ToDoTask> getTaskDetails(String userId, String taskId);
    void createTask(ToDoTask task);
    void updateTask(ToDoTask task);
    void deleteTask(String userId, String taskId);

}