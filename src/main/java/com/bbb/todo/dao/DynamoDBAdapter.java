package com.bbb.todo.dao;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.regions.Regions;
import com.amazonaws.retry.PredefinedRetryPolicies;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;

public class DynamoDBAdapter {
    private static DynamoDBAdapter dynamoAdapter = null;
    private AmazonDynamoDB client;
    private DynamoDBMapper mapper;

    private DynamoDBAdapter(String region){
        client = AmazonDynamoDBClientBuilder.standard()
                                            .withRegion(region)
                                            .withClientConfiguration(createDynamoDBClientConfiguration())
                                            .build();
    }

    public static DynamoDBAdapter getInstance(String region) {

        if ( null == region) {
            region = Regions.AP_SOUTH_1.getName();
        }

        if (dynamoAdapter == null) {
            dynamoAdapter = new DynamoDBAdapter(region);
        }
        return dynamoAdapter;
    }

    public AmazonDynamoDB getDynamoClient() {
        return this.client;
    }

    public DynamoDBMapper getMapper(DynamoDBMapperConfig mapperConfig) {
        if (this.client != null) {
            mapper = new DynamoDBMapper(this.client, mapperConfig);
        }
        return mapper;
    }
    private static ClientConfiguration createDynamoDBClientConfiguration() {
        ClientConfiguration clientConfiguration = new ClientConfiguration()
               .withConnectionTimeout(DynamoDBConfigs.CONNECTION_TIMEOUT)
                .withClientExecutionTimeout(DynamoDBConfigs.EXECUTION_TIMEOUT)
                .withRequestTimeout(DynamoDBConfigs.REQUEST_TIMEOUT)
                .withSocketTimeout(DynamoDBConfigs.SOCKET_TIMEOUT)
                .withRetryPolicy(PredefinedRetryPolicies.getDynamoDBDefaultRetryPolicyWithCustomMaxRetries(DynamoDBConfigs.RETRIES));
        return clientConfiguration;
    }
    
}