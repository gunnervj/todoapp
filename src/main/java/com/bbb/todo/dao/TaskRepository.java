package com.bbb.todo.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.TableNameOverride;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.bbb.exceptions.DataException;
import com.bbb.todo.dao.beans.ToDoTask;
import com.bbb.todo.model.ErrorConstants;
import com.bbb.todo.util.CleafDateUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TaskRepository implements ITaskRepository {
    private DynamoDBMapper mapper;
    
    public TaskRepository(String region, String tableName) {
        initDBMapperWithTableNameOverride(region, tableName);
    }

    public TaskRepository(String endpoint, Regions region) {
        initDBMapper(endpoint, region);
    }


    private void initDBMapperWithTableNameOverride(String region, String tableName) {
        log.info("Init Mapper With Region as " + region);
        System.out.println("Init Mapper With Region as " + region);
        System.out.println("Init Mapper With tableName as " + tableName);
        DynamoDBAdapter dbAdapter = DynamoDBAdapter.getInstance(region);
        DynamoDBMapperConfig mapperConfig = DynamoDBMapperConfig.builder().withTableNameOverride(new TableNameOverride(tableName)).build();
        this.mapper = dbAdapter.getMapper(mapperConfig);
    }

    private void initDBMapper(String endpoint, Regions region) {
        try {
            EndpointConfiguration endpointConfiguration = new EndpointConfiguration(endpoint, region.getName());
            AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
                                                            .withEndpointConfiguration(endpointConfiguration)
                                                            .build();
            this.mapper = new DynamoDBMapper(client);

        } catch (Exception ex) {
            log.error("Error while connecting to database: " + ex.getMessage(), ex);
            throw new DataException(ErrorConstants.PROCESSING_ERROR);
        }
    }

    @Override
    public Optional<List<ToDoTask>> getAllTasksForUser(String userId) {
        try {
            Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
            eav.put(":user_id", new AttributeValue().withS(userId));
            eav.put(":task_date", new AttributeValue().withS(CleafDateUtil.getTodaysDateAsString()));
            
            DynamoDBQueryExpression<ToDoTask> queryExpression = 
            new DynamoDBQueryExpression<ToDoTask>()
                    .withKeyConditionExpression("user_id = :user_id")
                    .withFilterExpression("task_date >= :task_date")
                    .withExpressionAttributeValues(eav);
            List<ToDoTask> tasks = mapper.query(ToDoTask.class, queryExpression);
            if (null != tasks && !tasks.isEmpty()) {
                return Optional.of(tasks);
            } 
        } catch (Exception ex) {
            log.error("Error while getting tasks from database: " + ex.getMessage(), ex);
            throw new DataException(ErrorConstants.PROCESSING_ERROR);
        }
        return Optional.empty();
    }

    @Override
    public Optional<ToDoTask> getTaskDetails(String userId, String taskId) {
        try {
            ToDoTask task = mapper.load(ToDoTask.class, userId, taskId);
            return Optional.of(task);
        } catch(Exception ex) {
            log.error("Error while getting task details from db: " + ex.getMessage(), ex);
            throw new DataException(ErrorConstants.PROCESSING_ERROR);
        }
    }

    @Override
    public void createTask(ToDoTask task) {
        try {
            mapper.save(task);
        } catch(Exception ex) {
            ex.printStackTrace();
            log.error("Error while creating task in db: " + ex.getMessage(), ex);
            throw new DataException(ErrorConstants.PROCESSING_ERROR);
        }
    }

    @Override
    public void updateTask(ToDoTask task) {
        try {
            mapper.save(task);
        } catch(Exception ex) {
            log.error("Error while updating task in db: " + ex.getMessage(), ex);
            throw new DataException(ErrorConstants.PROCESSING_ERROR);
        }
    }

    @Override
    public void deleteTask(String userId, String taskId) {
        try {
            ToDoTask task = new ToDoTask();
            task.setTaskId(taskId);
            task.setUserId(userId);
            mapper.delete(task);
        } catch(Exception ex) {
            log.error("Error while deleting task in db: " + ex.getMessage(), ex);
            throw new DataException(ErrorConstants.PROCESSING_ERROR);
        }
    }

     
}