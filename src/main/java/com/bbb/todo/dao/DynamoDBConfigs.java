package com.bbb.todo.dao;

public class DynamoDBConfigs {
    public static final int CONNECTION_TIMEOUT = 2000;
    public static final int EXECUTION_TIMEOUT = 5000;
    public static final int REQUEST_TIMEOUT = 1000;
    public static final int SOCKET_TIMEOUT = 1000;
    public static final int RETRIES = 3;
}