package com.bbb.todo;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.bbb.exceptions.DataException;
import com.bbb.todo.dao.ITaskRepository;
import com.bbb.todo.dao.TaskRepository;
import com.bbb.todo.dao.beans.ToDoTask;
import com.bbb.todo.model.ErrorConstants;
import com.bbb.todo.model.GetResponse;
import com.bbb.todo.model.HttpCodes;
import com.bbb.todo.model.Task;
import com.bbb.todo.util.CleafDateUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ToDoListRequestHandler implements RequestHandler<Map<String, String>, String> {
    private Gson gson;

    public ToDoListRequestHandler() {
        this.gson = new GsonBuilder().setPrettyPrinting().create();
    }

    @Override
    public String handleRequest(Map<String, String> event, Context context) {
        String regionDynamoDB = System.getenv().get(TaskConstants.DYNAMO_REGION);
        String tableNameDynamoDB =  System.getenv().get(TaskConstants.TASK_TABLE_NAME);
        List<ErrorConstants> errors = new ArrayList<>();
        GetResponse response = null;

        String taskId = event.get(TaskConstants.REQUEST_FIELD_NAME_TASK_ID);
        String userId = event.get(TaskConstants.REQUEST_FIELD_NAME_USER_ID);

        validateIncomingRequest(errors, event);

        try {
            ITaskRepository repository = new TaskRepository(regionDynamoDB, tableNameDynamoDB);
            if (null != taskId && taskId.trim().length() > 0) {
                Optional<ToDoTask> taskOpt = repository.getTaskDetails(userId, taskId);
                if (taskOpt.isPresent()) {
                    List<Task> taskList = new ArrayList<>();
                    ToDoTask task = taskOpt.get();
                    taskList.add(transformTask(task));

                    response = GetResponse.builder().status(HttpCodes.OK.getCode()).tasks(taskList).build();
                } else {
                    errors.add(ErrorConstants.PROCESSING_ERROR);
                    throw new RuntimeException(ErrorHandler.handleErrorResponse(HttpCodes.NOT_FOUND, errors));
                }

            } else {
                Optional<List<ToDoTask>> taskListOpt = repository.getAllTasksForUser(userId);
                if (taskListOpt.isPresent()) {
                    response = GetResponse.builder().status(HttpCodes.OK.getCode())
                            .tasks(groupCleafTasks(taskListOpt.get())).build();
                } else {
                    response = GetResponse.builder().status(HttpCodes.OK.getCode())
                            .tasks(groupCleafTasks(new ArrayList<>())).build();
                }

            }
        } catch (DataException ex) {
            errors.add(ex.getError());
            throw new RuntimeException(ErrorHandler.handleErrorResponse(HttpCodes.BAD_REQUEST, errors));
        } catch (Exception ex) {
            errors.add(ErrorConstants.PROCESSING_ERROR);
            throw new RuntimeException(ErrorHandler.handleErrorResponse(HttpCodes.INTERNALSERVER_ERROR, errors));
        }
        return gson.toJson(response);
    }

    private Task transformTask(ToDoTask cleafTask) {
        return Task.builder().description(cleafTask.getDescription()).status(cleafTask.getStatus())
                .taskDate(cleafTask.getTaskDate()).taskId(cleafTask.getTaskId()).title(cleafTask.getTitle()).build();
    }

    private Map<String, List<Task>> groupCleafTasks(List<ToDoTask> cleafTasks) {
        Map<String, List<Task>> taskMap = new HashMap<>();
        List<Task> todaysTasks = new ArrayList<>();
        List<Task> tomorrowsTasks = new ArrayList<>();
        List<Task> futureTasks = new ArrayList<>();
        taskMap.put(TaskConstants.TODAY, todaysTasks);
        taskMap.put(TaskConstants.TOMORROW, tomorrowsTasks);
        taskMap.put(TaskConstants.FUTURE, futureTasks);
        LocalDate today = LocalDate.now();
        LocalDate tomorrow = today.plus(1, ChronoUnit.DAYS);
        cleafTasks.forEach(cleafTask -> {
            Optional<LocalDate> taskDateOpt = CleafDateUtil.getDateStringAsLocalDate(cleafTask.getTaskDate());
            if (taskDateOpt.isPresent()) {
                if (taskDateOpt.get().isEqual(today)) {
                    taskMap.get(TaskConstants.TODAY).add(transformTask(cleafTask));
                } else if (taskDateOpt.get().isEqual(tomorrow)) {
                    taskMap.get(TaskConstants.TOMORROW).add(transformTask(cleafTask));
                } else if (taskDateOpt.get().isAfter(tomorrow)) {
                    taskMap.get(TaskConstants.FUTURE).add(transformTask(cleafTask));
                }
            }
        });

        return taskMap;
    }

    private void validateIncomingRequest(List<ErrorConstants> errors, Map<String, String> event) {
        if (null == event.get(TaskConstants.REQUEST_FIELD_NAME_USER_ID)
                || event.get(TaskConstants.REQUEST_FIELD_NAME_USER_ID).trim().length() == 0) {
            errors.add(ErrorConstants.UNAUTHORIZED);
            throw new RuntimeException(ErrorHandler.handleErrorResponse(HttpCodes.UNAUTHORIZED, errors));
        }
    }

}