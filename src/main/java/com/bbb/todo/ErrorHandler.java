package com.bbb.todo;

import java.util.ArrayList;
import java.util.List;

import com.bbb.todo.model.ErrorConstants;
import com.bbb.todo.model.ErrorResponse;
import com.bbb.todo.model.HttpCodes;
import com.bbb.todo.model.ToDoError;
import com.google.gson.Gson;

public class ErrorHandler {
    
   public static String handleErrorResponse(HttpCodes httpCode, List<ErrorConstants> errors) {
        Gson gson = new Gson();
        List<ToDoError> details = new ArrayList<>();
        
        for(ErrorConstants error : errors) {
            details.add(ToDoError.builder().code(error.getCode()).detail(error.getDetail()).message(error.getMessage()).build());

        }
        ErrorResponse response = ErrorResponse.builder().errorType(httpCode.getMessage())
                                                        .statusCode(httpCode.getCode())
                                                        .message(httpCode.getMessage())
                                                        .details(details)
                                                        .build();
        return gson.toJson(response);
    }
}