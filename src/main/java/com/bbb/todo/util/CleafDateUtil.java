package com.bbb.todo.util;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.TimeZone;

import com.bbb.todo.TaskConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CleafDateUtil {
    private static final Logger log = LoggerFactory.getLogger(CleafDateUtil.class);
    private static final String DATE_FORMAT_ISO_8601 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String TIME_ZONE = "UTC";

    public static String getTodaysDateAsString() {
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        LocalDate date = LocalDate.now();
        return date.format(dateFormatter) + TaskConstants.TIME_FORMAT;
    }

    public static String getYesterdaysDateAsString() {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT_ISO_8601);
        dateFormatter.setTimeZone(TimeZone.getTimeZone(TIME_ZONE));
        LocalDate today = LocalDate.now();
        LocalDate yesterday = today.minusDays(1);
        return dateFormatter.format(yesterday.toEpochDay());
    }
    
    public static Optional<LocalDate> getDateStringAsLocalDate(String dateString) {
        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT_ISO_8601);
            dateFormatter.setTimeZone(TimeZone.getTimeZone(TIME_ZONE));
            return Optional.of(dateFormatter.parse(dateString).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        } catch (Exception ex ) {
            log.error("Error while parsing date " + dateString, ex);
        }
        return Optional.empty();
    }
    

    public static boolean isDateStringValid(String dateString) {
        boolean isValid = true;
        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT_ISO_8601);
            dateFormatter.setTimeZone(TimeZone.getTimeZone(TIME_ZONE));
            dateFormatter.parse(dateString);
        } catch (Exception ex) {
            log.info("Error while parsing date " + dateString, ex);
            isValid = false;
        }

        return isValid;
    }

    public static boolean isDateInPast(String date) {
        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT_ISO_8601);
            dateFormatter.setTimeZone(TimeZone.getTimeZone(TIME_ZONE));
            LocalDate taskDate = dateFormatter.parse(date).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            return taskDate.isBefore(LocalDate.now());
        } catch (Exception ex ) {
            log.error("Error while parsing date " + date, ex);
        }
        return false;
    }
}