package com.bbb.todo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.bbb.exceptions.DataException;
import com.bbb.todo.dao.ITaskRepository;
import com.bbb.todo.dao.TaskRepository;
import com.bbb.todo.dao.beans.ToDoTask;
import com.bbb.todo.model.ErrorConstants;
import com.bbb.todo.model.HttpCodes;
import com.bbb.todo.model.ToDoCreateUpdateResponse;
import com.bbb.todo.model.ToDoStatus;
import com.bbb.todo.util.CleafDateUtil;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ToDoCreateRequestHandler implements RequestHandler<Map<String, String>, String> {
   
    private Gson gson;

    public ToDoCreateRequestHandler() {
        this.gson = new Gson();
    }

    @Override
    public String handleRequest(Map<String,String> event, Context context) {
        String regionDynamoDB =  System.getenv().get(TaskConstants.DYNAMO_REGION);
        String tableNameDynamoDB =  System.getenv().get(TaskConstants.TASK_TABLE_NAME);
        List<ErrorConstants> errors = new ArrayList<>();
        String response = "{}";
        log.info("Executing in Region : " + regionDynamoDB);
        validateIncomingRequest(errors, event);
        
        if (!errors.isEmpty()) {
            throw new RuntimeException(ErrorHandler.handleErrorResponse(HttpCodes.BAD_REQUEST, errors));
        }

        try {
            log.info("Getting DB connection : " + regionDynamoDB);
            ITaskRepository repository = new TaskRepository(regionDynamoDB, tableNameDynamoDB);
            log.info("Got DB connection : " + regionDynamoDB);
            ToDoTask task = new ToDoTask();
            task.setDescription(event.get(TaskConstants.REQUEST_FIELD_NAME_DESCRIPTION));
            task.setStatus(ToDoStatus.OPEN.name());
            task.setTaskDate(event.get(TaskConstants.REQUEST_FIELD_NAME_TASK_DATE));
            task.setTitle(event.get(TaskConstants.REQUEST_FIELD_NAME_TITLE));
            task.setUserId(event.get(TaskConstants.REQUEST_FIELD_NAME_USER_ID));
            task.setTaskId(UUID.randomUUID().toString());
            log.info("Creating task : " + task.toString());
            repository.createTask(task);
            response = gson.toJson(ToDoCreateUpdateResponse.builder().status(HttpCodes.CREATED.getCode())
                            .message(HttpCodes.CREATED.getMessage())
                            .task(task).build());
        } catch (DataException ex) {
            errors.add(ex.getError());
            throw new RuntimeException(ErrorHandler.handleErrorResponse(HttpCodes.INTERNALSERVER_ERROR, errors));
        }  catch (Exception ex) {
            errors.add(ErrorConstants.PROCESSING_ERROR);
            throw new RuntimeException(ErrorHandler.handleErrorResponse(HttpCodes.INTERNALSERVER_ERROR, errors));
        }
        log.info("Response = > " + response);
        return response;
    }

    private void validateIncomingRequest(List<ErrorConstants> errors, Map<String,String> event){
       
        if (null == event.get(TaskConstants.REQUEST_FIELD_NAME_USER_ID) || event.get(TaskConstants.REQUEST_FIELD_NAME_USER_ID).trim().length() == 0) {
            errors.add(ErrorConstants.UNAUTHORIZED);
            throw new RuntimeException(ErrorHandler.handleErrorResponse(HttpCodes.UNAUTHORIZED, errors));
        }
        if (null == event.get(TaskConstants.REQUEST_FIELD_NAME_TITLE) || event.get(TaskConstants.REQUEST_FIELD_NAME_TITLE).trim().length() == 0) {
            errors.add(ErrorConstants.TITLE_MISSING);
        }
        if (null == event.get(TaskConstants.REQUEST_FIELD_NAME_DESCRIPTION) || event.get(TaskConstants.REQUEST_FIELD_NAME_DESCRIPTION).trim().length() == 0) {
            errors.add(ErrorConstants.DESCRIPTION_MISSING);
        }
        
        String taskDate = event.get(TaskConstants.REQUEST_FIELD_NAME_TASK_DATE);
        if (null == taskDate || taskDate.trim().length() == 0) {
            errors.add(ErrorConstants.TASK_DATE_MISSING);
        } else {
            taskDate = taskDate.trim() + TaskConstants.TIME_FORMAT;
            event.put(TaskConstants.REQUEST_FIELD_NAME_TASK_DATE, taskDate);
        }

        if (!CleafDateUtil.isDateStringValid(taskDate)) {
                errors.add(ErrorConstants.TASK_DATE_INVALID_FORMAT);
        }
        if (CleafDateUtil.isDateInPast(taskDate)) {
            errors.add(ErrorConstants.TASK_DATE_INVALID_PAST);
        }
        
    }
    
}