package com.bbb.todo.model;

import java.util.List;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class GetResponse {
    private Integer status;
    private Object tasks;
}