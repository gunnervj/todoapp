package com.bbb.todo.model;

import lombok.Getter;

@Getter
public enum HttpCodes {
    OK("OK", 200),
    CREATED("CREATED", 201),
    NOT_FOUND("NotFound", 404),
    BAD_REQUEST("Bad Request", 400),
    UNAUTHORIZED("Unauthorized", 401),
    INTERNALSERVER_ERROR("Internal Error", 500);

    HttpCodes(String message, Integer code){
        this.code = code;
        this.message = message;
    }

    private String message;
    private Integer code;
}