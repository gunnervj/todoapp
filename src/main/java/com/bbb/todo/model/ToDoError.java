package com.bbb.todo.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ToDoError {
    private Integer code;
    private String message;
    private String detail;
}