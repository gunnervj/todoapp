package com.bbb.todo.model;

import com.bbb.todo.dao.beans.ToDoTask;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ToDoCreateUpdateResponse {
    private Integer status;
    private String message;
    private ToDoTask task;    
}