package com.bbb.todo.model;

import java.util.List;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ErrorResponse {
    private String errorType;
    private Integer statusCode;
    private String message;
    private List<ToDoError> details;
}