package com.bbb.todo.model;

public enum ToDoStatus {
    COMPLETED,
    ERROR,
    OPEN;

    public static String translateStatus(String status) {
        if (null != status && status.trim().length() > 0) {
            status = status.trim();
            for (ToDoStatus todoStatus : ToDoStatus.values()) {
                if (todoStatus.name().equals(status.toUpperCase())) {
                    return todoStatus.name();
                }
            }
            return ERROR.name();
        } else {
            return "";
        }
    }

    public static String possibleValues() {
        String possibleValues = "";
        for (ToDoStatus todoStatus : ToDoStatus.values()) {
            possibleValues = possibleValues + todoStatus.name() + " | ";
        }
        return possibleValues.trim().substring(0, possibleValues.trim().lastIndexOf("|"));
    }


}