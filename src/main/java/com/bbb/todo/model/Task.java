package com.bbb.todo.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Task {
    private String taskId;
    private String description;
    private String title;
    private String status;
    private String taskDate;
}