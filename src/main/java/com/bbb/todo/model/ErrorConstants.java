package com.bbb.todo.model;

import lombok.Getter;

@Getter
public enum ErrorConstants {
    UNAUTHORIZED(99, "Unauthorized.", "You are not authorzied"),
    TITLE_MISSING(100, "Task title is missing.", "Task title is missing or is empty."),
    DESCRIPTION_MISSING(101, "Task description is missing." , "Task description is missing or is empty."),
    TASK_DATE_MISSING(102, "Task Date is missing.", "Task Date is missing or is empty."),
    TASK_DATE_INVALID_FORMAT(103, "Task Date is invalid.", "Task Date is in invalid format. Example format (2020-08-15)"),
    TASK_DATE_INVALID_PAST(104, "Task Date is invalid.", "Task cannot be created for a past date."),
    TASK_MISSING(105,"Task Id is missing", "Task Id is missing."),
    TASK_NOT_FOUND(106, "Task Not Found" , "Requested task not found"),
    INVALID_STATUS(107, "Invalid Status", "Invalid Status for a task. Possible values are : " + ToDoStatus.possibleValues()),
    PROCESSING_ERROR(500, "Error while Processing" ,"Something went wrong while processing. Try again. If probelm persist, contact helpdesk.");


    private int code;
    private String message;
    private String detail;
    ErrorConstants(int code, String message, String detail) {
        this.code = code;
        this.message = message;
        this.detail = detail;
    }


}